## An extremely inadequate watchlater replacement because Youtube is a pile of garbage.

Use cargo to compile.

Create a watch later folder.
Use Lbry client or yt-dlp to download videos you want to watch.
Run this program from your watch later folder.
It will launch vlc at 1.75x speed (you can modify this in code) and play the smallest video in the folder.
Upon completion, it will add that video name to the watched list, then play the next video.
Press "Y" in the terminal window to delete all watched videos.
Press "Q" to exit, this will not close the existing VLC.
You can continue copying or downloading videos into this folder while the script is running, it will update the list every time a video is completed. Make sure the tool you use to download doesn't leave .mp4 or .webm videos in the folder that are partially downloaded. (this means the LBRY desktop client, you should download to a different folder then move them into the watch later folder when downloading is completed. yt-dlp doesn't have that issue.)

### Why on earth does this exist???

I am very particular about how I like my videos sorted, and youtube's interface keeps erroring out when I rearrange things, then kicks them back where they were before. Because of this, even though I pay for Youtube Premium, Youtube is a terrible experience. 

### Future enhancements

add support for reading the length of webm and mp4 videos, and sort by that length instead of the video size. The video size is an unreliable indicator of how long the video is, and I prefer to watch shortest to longest.
