use std::fs;
use std::fs::{Metadata, DirEntry};
use std::path::Path;
use std::process::Command;
use console::Term;
use std::ffi::{OsString};
use std::sync::{RwLock, Arc};
use std::collections::HashSet;
use std::thread;

#[derive(Debug)]
enum VideoType {
    MP4(),
    WebM(),
}

#[derive(Debug)]
struct Video {
    path: Box<Path>,
    metadata: Metadata,
    vidtype: VideoType,
}

fn extract_path_video(path: DirEntry, vecpath: &mut Vec<Video>) {
    let unpath = path.path();
    let metadata = path.metadata().unwrap();
    if unpath.is_file() {
        match unpath.extension() {
            Some(extension) => {
                match extension.to_str() {
                    Some("mp4") => vecpath.push(Video {
                        path: unpath.into(),
                        metadata: metadata,
                        vidtype: VideoType::MP4()
                    }),
                    Some("webm") => vecpath.push(Video {
                        path: unpath.into(),
                        metadata: metadata,
                        vidtype: VideoType::WebM()
                    }),
                    _ => (),
                }
            },
            _ => (),
        }
    }
}

fn get_videos(path: &str) -> Vec<Video> {
    let paths = fs::read_dir(path).unwrap();
    let mut vecpath = Vec::<Video>::new();
    for path in paths {
        if let Ok(path2) = path {
            extract_path_video(path2, &mut vecpath);
        }
    }
    vecpath
}

fn prompt_delete(watched_pile: Arc<RwLock<HashSet<OsString>>>, termsig: Arc<RwLock<Box<bool>>>) {
    let stdout = Term::buffered_stdout();
    if let Err(err) = stdout.flush() {
        println!("Flush failed. {}", err);
    }
    'prompt_loop: loop {
        if let Ok(character) = stdout.read_char() {
            match character {
                'Y' | 'y' => {
                    println!("Should delete file here.");
                    let mut hash = watched_pile.write().unwrap();
                    for file in hash.iter() {
                        println!("I would delete {:?} here.", file);
                        if let Err(x) = fs::remove_file(file) {
                            println!("Failed to delete {} {}", file.to_str().unwrap(), x);
                        }
                    }
                    hash.clear();
                },
                'n' | 'N' => {
                    println!("Ok, we won't delete it.");
                },
                'q' | 'Q' => {
                    println!("Should terminate here.");
                    **termsig.write().unwrap() = false;
                    break 'prompt_loop
                },
            _ => (),
            }
        }
    }
}

fn find_unplayed_smallest_video(path: &str, watched_pile: &Arc<RwLock<HashSet<OsString>>>) -> Option<OsString> {
    let mut vecpath = get_videos(path);
    vecpath.sort_by(|a, b| a.metadata.len().cmp(&b.metadata.len()));
    let mut my_path: Option<OsString> = None;
    let watched = watched_pile.read().unwrap();
    for video in vecpath {
        //println!("Video: {} {} {:?}", video.metadata.len(), video.path.display(), video.vidtype);
        if my_path != None || watched.contains(&*video.path.as_os_str()) {
            // Skip.
        } else {
            println!("Selecting Video: {:?} {} {}", video.vidtype, video.metadata.len(), video.path.display());
            let p = video.path.as_os_str().clone();
            my_path = Some(p.to_os_string());
        }
    }
    my_path
}

fn play_thread(path: &str, watched_pile: Arc<RwLock<HashSet<OsString>>>, termsig: Arc<RwLock<Box<bool>>>) {
    let path2 = String::from(path);
    thread::spawn(move || {
        let mut alive = **termsig.read().unwrap();
        while alive {
            let tmppath = find_unplayed_smallest_video(&path2, &watched_pile);
            match tmppath {
                Some(my_path) => {
                    if let Err(x) = Command::new("vlc")
                        .arg("--play-and-exit")
                        .arg("--rate").arg("1.75")
                        .arg(&my_path)
                        .status() {
                            println!("Error running vlc {:?}", x);
                    } else {
                        let mut hash = watched_pile.write().unwrap();
                        hash.insert(my_path.to_os_string());
                        println!("Watched videos: ");
                        for file in hash.iter() {
                            println!("{:?}", file);
                        }
                        println!("Added video to watched list. Press Y to delete all watched videos, Q to quit.");
                    }
                    alive = **termsig.read().unwrap();
                },
                None => {
                    alive = false;
                    println!("No more videos to play, ending vlc thread.");
                }
            }
        }
    });
}

fn main() {
    let endthread = Arc::new(RwLock::new(Box::new(true)));
    let watched_pile = Arc::new(RwLock::new(HashSet::<OsString>::new()));
    play_thread("./", watched_pile.clone(), endthread.clone());
    prompt_delete(watched_pile, endthread);
}
